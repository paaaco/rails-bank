require "rails_helper"

RSpec.describe BankTransactionsController, type: :routing do
  describe "routing" do
    it "routes to #new" do
      expect(get: "/bank_transactions/new").to route_to("bank_transactions#new")
    end

    it "routes to #create" do
      expect(post: "/bank_transactions").to route_to("bank_transactions#create")
    end
  end
end
