require "rails_helper"

RSpec.describe BankAccountsController, type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(get: "/bank_account").to route_to("bank_accounts#show")
    end
  end
end
