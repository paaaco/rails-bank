FactoryBot.define do
  factory :bank_account do
    sequence(:account_number) { |n| "#{n}".rjust(12, "0") }
    user
  end
end