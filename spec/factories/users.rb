FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@email.test" }
    name { "Test User" }
    password { "aaaaaaaaaaa" }
  end
end