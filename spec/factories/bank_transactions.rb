FactoryBot.define do
  factory :bank_transaction do
    transaction_type { :debit }
    amount { 1 }
    bank_account
  end
end