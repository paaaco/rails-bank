require 'rails_helper'

RSpec.describe BankAccount, type: :model do
  context "when a bank account is created" do
    let(:bank_account) { build(:bank_account, account_number: nil) }
    
    it "should generate a unique 12-digit account number" do 
      bank_account.save 
      expect(bank_account.reload.account_number).to match(/\d{12}/)  
    end

    context "if there's already an existing bank account" do 
      let!(:old_bank_account) { create(:bank_account, account_number: "123456789012") }

      it "new bank account should not be allowed to have the existing account number" do 
        bank_account.account_number = "123456789012"
        expect(bank_account).to_not be_valid
      end
    end
  end
  
  describe "Model Methods" do 
    describe "#transfer" do
      let(:bank_account_1){ create(:bank_account, balance: 1_000) }
      let(:bank_account_2){ create(:bank_account, balance: 1) }
      let(:target_bank_account_number){ bank_account_2.account_number }
      let(:transfer_amount){ 1_000 }

      subject do 
        bank_account_1.transfer(target_bank_account_number, transfer_amount)
      end

      it "should create a transaction for the source and the destination bank accounts" do 
        expect{ subject }.to change{ BankTransaction.count }.by(2)
      end

      it "should create one debit transaction for the source" do 
        expect{ subject }.to change{ BankTransaction.debit.count }.by(1)
      end

      it "should create one credit transaction for the destination" do 
        expect{ subject }.to change{ BankTransaction.credit.count }.by(1)
      end

      it "should update balance of source bank account" do 
        subject
        expect(bank_account_1.reload.balance).to eq(0)
      end

      it "should update balance of destination bank account" do 
        subject
        expect(bank_account_2.reload.balance).to eq(1_001)
      end

      context "when source account has less balance than it plans to transfer" do
        let(:transfer_amount){ 1_001 }

        it "should not create any transaction" do 
          expect{ subject }.to change{ BankTransaction.debit.count }.by(0)
        end
      end

      context "when a non-existent account number is provided" do 
        let(:target_bank_account_number){ "000000000000" }

        it "should not create any transaction" do 
          expect{ subject }.to change{ BankTransaction.debit.count }.by(0)
        end
      end
    end
  end
end
