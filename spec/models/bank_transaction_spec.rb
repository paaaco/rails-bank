require 'rails_helper'

RSpec.describe BankTransaction, type: :model do
  let(:bank_transaction) { build(:bank_transaction, bank_account: bank_account, transaction_type: transaction_type, amount: amount) }
  
  describe "On creation" do 
    before :each do 
      bank_transaction.save
    end

    context "when a bank transaction is made, it should update bank account balance" do 
      let(:bank_account) { create(:bank_account, balance: 1_000) }
      let(:amount) { 250 }

      context "when transaction is debit" do 
        let(:transaction_type) { :debit }

        it "should decrease the related bank account's balance" do 
          expect(bank_transaction.bank_account.balance).to eq(750)
        end
      end

      context "when transaction is credit" do 
        let(:transaction_type) { :credit }

        it "should decrease the related bank account's balance" do
          expect(bank_transaction.bank_account.balance).to eq(1_250)
        end
      end
    end

    context "When multiple transactions are made" do 
      let(:bank_account) { create(:bank_account, balance: 1_000) }
      let(:amount) { 0 }
      let(:transaction_type) { :debit }

      let(:transaction_history) { [ [:debit, 100, 900], [:credit, 110, 1_010], [:debit, 116, 894], [:debit, 28, 866] ] }


      it "should save running_balance on every step" do                
        transaction_history.each do |transaction, amount, expected_balance|
          new_bank_transaction = create(:bank_transaction, 
            bank_account: bank_account, 
            amount: amount, 
            transaction_type: transaction
          )

          expect(new_bank_transaction.running_balance).to eq(expected_balance)
        end
      end
    end
  end

  describe "Validations" do
    context "when a debit is made that is more than the account's current balance" do 
      let(:transaction_type) { :debit }
      let(:amount) { 1000 }
      let(:bank_account) { create(:bank_account, balance: 999) }

      it "should not be valid" do 
        expect(bank_transaction).to_not be_valid
      end
    end
  end

  describe "Model methods" do 
    let(:bank_account) { create(:bank_account, balance: 1_000) }

    describe "#debit_amount" do
      let(:amount){ 100 }

      subject do 
        bank_transaction.debit_amount
      end

      context "transaction type is debit" do 
        let(:transaction_type){ :debit }
        
        it "should return amount debitted" do 
          expect(subject).to eq(100)
        end
      end 

      context "transaction type is credit" do 
        let(:transaction_type){ :credit }

        it "should return nothing" do 
          expect(subject).to eq(nil)
        end
      end
    end

    describe "#credit_amount" do
      let(:amount){ 100 }

      subject do 
        bank_transaction.credit_amount
      end

      context "transaction type is debit" do 
        let(:transaction_type){ :debit }
        
        it "should return nothing" do 
          expect(subject).to eq(nil)
        end
      end 

      context "transaction type is credit" do 
        let(:transaction_type){ :credit }

        it "should return amount credited" do 
          expect(subject).to eq(100)
        end
      end
    end
  end
end
