require 'rails_helper'

RSpec.describe User, type: :model do
  context "when a user is created" do
    let(:user){ build(:user) } 

    it "a bank account should be created as well" do 
      expect{ user.save }.to change{ BankAccount.count }.by(1)
    end
  end

  context "Model methods" do
    let(:user) { create(:user) }
    
    describe "add_funds" do 
      subject do 
        user.add_funds(100)
      end

      it "should add directly change balance of user's bank account" do
        subject
        expect(user.bank_account.balance).to eq(100) 
      end

      it "should not generate additional bank transaction records" do 
        expect{ subject }.to change{ BankTransaction.count }.by(0)
      end
    end 
  end
end
