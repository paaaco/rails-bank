require 'rails_helper'

RSpec.describe "bank_transactions/new", type: :view do
  let(:bank_account) { create :bank_account }
  
  before(:each) do
    assign(:bank_account, bank_account)
    assign(:bank_transaction, BankTransaction.new(
      bank_account: bank_account
    ))
  end

  it "renders new bank_transaction form" do
    render

    assert_select "form[action=?][method=?]", bank_transactions_path, "post" do
      assert_select "input[name=?]", "bank_transaction[bank_account[account_number]]"
      assert_select "input[name=?]", "bank_transaction[amount]"
    end
  end
end
