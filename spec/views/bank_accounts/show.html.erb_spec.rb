require 'rails_helper'

RSpec.describe "bank_accounts/show", type: :view do
  let(:user){ create :user }

  before :each do 
    sign_in user
    assign(:bank_account, user.bank_account)
    assign(:transactions, user.bank_account.bank_transactions)
  end
  
  it "renders attributes" do
    render
  end
end
