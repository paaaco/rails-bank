Rails.application.routes.draw do
  resources :bank_transactions, only: [:create, :new]
  get "/bank_account", to: "bank_accounts#show"
  devise_for :users

  root "bank_accounts#show"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
