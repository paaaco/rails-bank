class CreateBankAccounts < ActiveRecord::Migration[6.1]
  def change
    create_table :bank_accounts do |t|
      t.string :account_number
      t.decimal :balance, precision: 8, scale: 2, default: 0
      t.references :user

      t.timestamps
    end
  end
end
