class CreateBankTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :bank_transactions do |t|
      t.references :bank_account, null: false, foreign_key: true
      t.decimal :amount, precision: 8, scale: 2
      t.decimal :running_balance, precision: 8, scale: 2
      t.integer :transaction_type

      t.timestamps
    end
  end
end
