user = User.create(name: "Paco", email: "paco@paco.test", password: "password")
bank_account = user.reload.bank_account

bank_account.bank_transactions.credit.create(amount: 42)
sleep 1
bank_account.bank_transactions.credit.create(amount: 100)
sleep 1
bank_account.bank_transactions.debit.create(amount: 25)
sleep 1
bank_account.bank_transactions.debit.create(amount: 75)

# Create Another User
user_2 = User.create(name: "Other User", email: "other@user.test", password: "password")
bank_account_2 = user_2.reload.bank_account

bank_account_2.bank_transactions.credit.create(amount: 999_999)

# Add some transfers
bank_account.transfer(bank_account_2.account_number, 10)
bank_account_2.transfer(bank_account.account_number, 96)