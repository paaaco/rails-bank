# README

## Rails Banking App

### Project notes

* run seeds with `rails db:seed` for some initial users and transactions
* to create a new User through console, you need to provide `name`, `email`, `password`.
  * e.g. `User.create(name: "Paco", email: "paco@example.test", password: "password")`
  * upon creation, a `BankAccount` with 0 balance will be automatically generated and assigned to this user.
* Each `BankAccount` will have a randomly-generated 12-digit account number that is used when transferring funds.
* To add funds to a user via console, run `@user.add_funds(amount)`.
  * This adds funds directly to the account without generating transactions for it.

### Possible Expansions

* The system currently supports only one `BankAccount` per `User`, however DB schema allows for possible expansions to allow multiple Bank accounts for each user.

### Testing

* This app's automated testing framework is RSpec, to run tests just run `rspec`.
