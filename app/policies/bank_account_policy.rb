class BankAccountPolicy < ApplicationPolicy
  def show?
    record.user == user
  end
end