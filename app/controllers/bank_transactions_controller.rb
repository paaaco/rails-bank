class BankTransactionsController < ApplicationController
  before_action :set_bank_transaction, only: %i[ show ]
  before_action :set_bank_account

  # GET /bank_transactions/1 or /bank_transactions/1.json
  def show
  end

  # GET /bank_transactions/new
  def new
    @bank_transaction = BankTransaction.new
  end

  # POST /bank_transactions or /bank_transactions.json
  def create
    @bank_transaction = BankTransaction.new
    target_account = bank_transaction_params[:bank_account][:account_number]
    amount = bank_transaction_params[:amount].to_f

    respond_to do |format|
      if @bank_account.transfer(target_account, amount)
        format.html { redirect_to bank_account_path, notice: "Bank transaction was successfully created." }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  private
  def set_bank_transaction
    @bank_transaction = BankTransaction.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def bank_transaction_params
    params.require(:bank_transaction).permit(:amount, bank_account: [ :account_number ] )
  end
end
