class BankAccountsController < ApplicationController
  before_action :set_bank_account, only: %i[ show ]

  # GET /bank_accounts/1 or /bank_accounts/1.json
  def show
    @transactions = @bank_account.bank_transactions.order(created_at: :desc)
  end
end
