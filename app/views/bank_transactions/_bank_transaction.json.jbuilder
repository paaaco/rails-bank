json.extract! bank_transaction, :id, :bank_account_id, :created_at, :updated_at
json.url bank_transaction_url(bank_transaction, format: :json)
