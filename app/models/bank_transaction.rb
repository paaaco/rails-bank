class BankTransaction < ApplicationRecord
  belongs_to :bank_account

  enum transaction_type: { debit: 0, credit: 1 }

  before_save :update_account_balance
  before_validation :check_for_minimum_balance

  def credit_amount
    return nil if debit?
    amount
  end

  def debit_amount
    return nil if credit?
    amount
  end

  private

  def update_account_balance
    if debit?
      self.running_balance = bank_account.balance - amount
      bank_account.update(balance: running_balance)
    elsif credit?
      self.running_balance = bank_account.balance + amount
      bank_account.update(balance: running_balance)
    else
      self.errors.add(:transaction_type, "Unknown transaction type")
      raise ActiveRecord::RecordInvalid.new(self)
    end
  end

  def check_for_minimum_balance
    if debit?
      if amount > bank_account.balance 
        self.errors.add(:amount, "Not enough money in bank account")
      end
    end
  end
end
