class BankAccount < ApplicationRecord
  belongs_to :user
  has_many :bank_transactions

  validates :account_number, uniqueness: true

  before_save :generate_account_number, if: Proc.new { account_number.nil? }  

  def transfer(destination_account_number, amount)
    ActiveRecord::Base.transaction do
      destination = BankAccount.find_by(account_number: destination_account_number)

      if validate_destination(destination) && validate_account_balance(amount)
        self.bank_transactions.debit.create(amount: amount)
        destination.bank_transactions.credit.create(amount: amount)
      end 
    end
  end

  private 

  def generate_account_number
    number_generator = -> { Array.new(12) { (0..9).to_a.sample }.join }
    generated_account_number = number_generator.call

    while BankAccount.where(account_number: generated_account_number).first do
      generated_account_number = number_generator.call
    end

    self.account_number = generated_account_number
  end

  def validate_account_balance(amount)
    return true if balance >= amount
    self.errors.add(:balance, "not enough")
    return false
  end

  def validate_destination(destination)
    return true if destination 
    self.errors.add(:base, "Target account not found") 
    return false
  end
end
