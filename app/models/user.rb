class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  has_one :bank_account
  has_many :bank_transactions, through: :bank_account

  validates :name, presence: true

  after_create :create_bank_account, if: Proc.new { bank_account.nil? }

  def add_funds(amount)
    self.bank_account.update(balance: bank_account.balance + amount)
  end
end
